package com.atlassian.jira.plugin.editor.ref;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Map;
import java.util.Optional;

public class FooterMacro extends BaseMacro {
    @Override
    public boolean hasBody() {
        return true;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.allow(RenderMode.F_ALL);
    }

    @Override
    public String execute(Map<String, Object> parameters, String body, RenderContext renderContext) throws MacroException {
        return String.format("<div class=\"footer-macro\"%s>%s</div>",
                Optional.ofNullable(parameters.getOrDefault("title", null))
                        .map(title -> String.format(" title=\"%s\"", StringEscapeUtils.escapeHtml4((String) title)))
                        .orElse(""),
                body);
    }
}
